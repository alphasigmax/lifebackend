package com.lifestyle.domain.wrapper;

import java.util.List;

import com.lifestyle.domain.Answer;
import com.lifestyle.domain.User;

public class SurveyResultWrapper {

	private String sid;

	private List<Answer> answers = null;
	
	private User user = null;

	private String feedback1score = null;
	
	private String feedback2score = null;
	
	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getFeedback1score() {
		return feedback1score;
	}

	public void setFeedback1score(String feedback1score) {
		this.feedback1score = feedback1score;
	}

	public String getFeedback2score() {
		return feedback2score;
	}

	public void setFeedback2score(String feedback2score) {
		this.feedback2score = feedback2score;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "SurveyResultWrapper [answers=" + answers + ", user=" + user
				+ "]";
	}
	
	
}
