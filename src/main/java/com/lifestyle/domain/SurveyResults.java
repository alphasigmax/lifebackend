package com.lifestyle.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SurveyResults.
 */
@Entity
@Table(name = "survey_results")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SurveyResults implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "user_id")
    private Long user_id;

    @Column(name = "answer_id")
    private Long answer_id;

    @Column(name = "question_id")
    private Long question_id;

    @Column(name = "category_id")
    private Long category_id;

    @Column(name = "survey_id")
    private Long survey_id;

    @Column(name = "feed_back_one")
    private String feedBackOne;

    @Column(name = "feed_back_two")
    private String feedBackTwo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getAnswer_id() {
        return answer_id;
    }

    public void setAnswer_id(Long answer_id) {
        this.answer_id = answer_id;
    }

    public Long getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(Long question_id) {
        this.question_id = question_id;
    }

    public Long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Long category_id) {
        this.category_id = category_id;
    }

    public Long getSurvey_id() {
		return survey_id;
	}

	public void setSurvey_id(Long survey_id) {
		this.survey_id = survey_id;
	}

	public String getFeedBackOne() {
        return feedBackOne;
    }

    public void setFeedBackOne(String feedBackOne) {
        this.feedBackOne = feedBackOne;
    }

    public String getFeedBackTwo() {
        return feedBackTwo;
    }

    public void setFeedBackTwo(String feedBackTwo) {
        this.feedBackTwo = feedBackTwo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SurveyResults surveyResults = (SurveyResults) o;
        if(surveyResults.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, surveyResults.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SurveyResults{" +
            "id=" + id +
            ", user_id='" + user_id + "'" +
            ", answer_id='" + answer_id + "'" +
            ", question_id='" + question_id + "'" +
            ", category_id='" + category_id + "'" +
            ", survey_id='" + survey_id + "'" +
            ", feedBackOne='" + feedBackOne + "'" +
            ", feedBackTwo='" + feedBackTwo + "'" +
            '}';
    }
}
