package com.lifestyle.domain.enumeration;

/**
 * The Scorelevel enumeration.
 */
public enum Scorelevel {
    HIGH,MEDIUM,LOW
}
