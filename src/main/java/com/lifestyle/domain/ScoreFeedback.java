package com.lifestyle.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.lifestyle.domain.enumeration.Scorelevel;

/**
 * A ScoreFeedback.
 */
@Entity
@Table(name = "score_feedback")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ScoreFeedback implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "score_level_one")
    private Scorelevel scoreLevelOne;

    @Enumerated(EnumType.STRING)
    @Column(name = "score_level_two")
    private Scorelevel scoreLevelTwo;

    @Size(max = 15000)
    @Column(name = "user_feed_back", length = 15000)
    private String userFeedBack;

    @OneToOne
    @JoinColumn(unique = true)
    private Category category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Scorelevel getScoreLevelOne() {
        return scoreLevelOne;
    }

    public void setScoreLevelOne(Scorelevel scoreLevelOne) {
        this.scoreLevelOne = scoreLevelOne;
    }

    public Scorelevel getScoreLevelTwo() {
        return scoreLevelTwo;
    }

    public void setScoreLevelTwo(Scorelevel scoreLevelTwo) {
        this.scoreLevelTwo = scoreLevelTwo;
    }

    public String getUserFeedBack() {
        return userFeedBack;
    }

    public void setUserFeedBack(String userFeedBack) {
        this.userFeedBack = userFeedBack;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ScoreFeedback scoreFeedback = (ScoreFeedback) o;
        if(scoreFeedback.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, scoreFeedback.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ScoreFeedback{" +
            "id=" + id +
            ", scoreLevelOne='" + scoreLevelOne + "'" +
            ", scoreLevelTwo='" + scoreLevelTwo + "'" +
            ", userFeedBack='" + userFeedBack + "'" +
            '}';
    }
}
