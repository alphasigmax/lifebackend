package com.lifestyle.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Category.
 */
@Entity
@Table(name = "category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "category_name")
    private String categoryName;

    @Column(name = "number_of_questions_per_page")
    private Integer numberOfQuestionsPerPage;

    @Column(name = "question_heading")
    private String questionHeading;

    @Column(name = "category_group_id")
    private Integer categoryGroupId;

    @Column(name = "pre_text")
    private String preText;

    @Column(name = "post_text")
    private String postText;

    @OneToMany(mappedBy = "category")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Question> questions = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getNumberOfQuestionsPerPage() {
        return numberOfQuestionsPerPage;
    }

    public void setNumberOfQuestionsPerPage(Integer numberOfQuestionsPerPage) {
        this.numberOfQuestionsPerPage = numberOfQuestionsPerPage;
    }

    public String getQuestionHeading() {
        return questionHeading;
    }

    public void setQuestionHeading(String questionHeading) {
        this.questionHeading = questionHeading;
    }

    public Integer getCategoryGroupId() {
        return categoryGroupId;
    }

    public void setCategoryGroupId(Integer categoryGroupId) {
        this.categoryGroupId = categoryGroupId;
    }

    public String getPreText() {
        return preText;
    }

    public void setPreText(String preText) {
        this.preText = preText;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Category category = (Category) o;
        if(category.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, category.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Category{" +
            "id=" + id +
            ", categoryName='" + categoryName + "'" +
            ", numberOfQuestionsPerPage='" + numberOfQuestionsPerPage + "'" +
            ", questionHeading='" + questionHeading + "'" +
            ", categoryGroupId='" + categoryGroupId + "'" +
            ", preText='" + preText + "'" +
            ", postText='" + postText + "'" +
            '}';
    }
}
