package com.lifestyle.repository;

import com.lifestyle.domain.Question;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Question entity.
 */
public interface QuestionRepository extends JpaRepository<Question,Long> {

    @Query(value = "select * from question where category_id in (select id from category where category_group_id =?1)", nativeQuery = true)
    List<Question> findAllByGroup(Long categoryGroupId);
}
