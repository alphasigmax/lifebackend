package com.lifestyle.repository;

import com.lifestyle.domain.SurveyResults;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the SurveyResults entity.
 */
public interface SurveyResultsRepository extends JpaRepository<SurveyResults,Long> {

}
