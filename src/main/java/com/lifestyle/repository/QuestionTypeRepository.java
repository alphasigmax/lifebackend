package com.lifestyle.repository;

import com.lifestyle.domain.QuestionType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the QuestionType entity.
 */
public interface QuestionTypeRepository extends JpaRepository<QuestionType,Long> {

}
