package com.lifestyle.repository;

import com.lifestyle.domain.ScoreFeedback;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ScoreFeedback entity.
 */
public interface ScoreFeedbackRepository extends JpaRepository<ScoreFeedback,Long> {

}
