package com.lifestyle.repository;

import com.lifestyle.domain.Answer;

import org.springframework.data.jpa.repository.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Spring Data JPA repository for the Answer entity.
 */
public interface AnswerRepository extends JpaRepository<Answer,Long> {
	@Transactional
	@Modifying
	@Query("delete from Answer a where a.id>0")
	void deleteById();
	
}
