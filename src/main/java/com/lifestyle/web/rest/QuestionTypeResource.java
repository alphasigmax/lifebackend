package com.lifestyle.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.lifestyle.domain.QuestionType;
import com.lifestyle.repository.QuestionTypeRepository;
import com.lifestyle.web.rest.util.HeaderUtil;
import com.lifestyle.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing QuestionType.
 */
@RestController
@RequestMapping("/api")
public class QuestionTypeResource {

    private final Logger log = LoggerFactory.getLogger(QuestionTypeResource.class);
        
    @Inject
    private QuestionTypeRepository questionTypeRepository;
    
    /**
     * POST  /question-types : Create a new questionType.
     *
     * @param questionType the questionType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new questionType, or with status 400 (Bad Request) if the questionType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/question-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<QuestionType> createQuestionType(@RequestBody QuestionType questionType) throws URISyntaxException {
        log.debug("REST request to save QuestionType : {}", questionType);
        if (questionType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("questionType", "idexists", "A new questionType cannot already have an ID")).body(null);
        }
        QuestionType result = questionTypeRepository.save(questionType);
        return ResponseEntity.created(new URI("/api/question-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("questionType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /question-types : Updates an existing questionType.
     *
     * @param questionType the questionType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated questionType,
     * or with status 400 (Bad Request) if the questionType is not valid,
     * or with status 500 (Internal Server Error) if the questionType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/question-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<QuestionType> updateQuestionType(@RequestBody QuestionType questionType) throws URISyntaxException {
        log.debug("REST request to update QuestionType : {}", questionType);
        if (questionType.getId() == null) {
            return createQuestionType(questionType);
        }
        QuestionType result = questionTypeRepository.save(questionType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("questionType", questionType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /question-types : get all the questionTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of questionTypes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/question-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<QuestionType>> getAllQuestionTypes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of QuestionTypes");
        Page<QuestionType> page = questionTypeRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/question-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /question-types/:id : get the "id" questionType.
     *
     * @param id the id of the questionType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the questionType, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/question-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<QuestionType> getQuestionType(@PathVariable Long id) {
        log.debug("REST request to get QuestionType : {}", id);
        QuestionType questionType = questionTypeRepository.findOne(id);
        return Optional.ofNullable(questionType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /question-types/:id : delete the "id" questionType.
     *
     * @param id the id of the questionType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/question-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteQuestionType(@PathVariable Long id) {
        log.debug("REST request to delete QuestionType : {}", id);
        questionTypeRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("questionType", id.toString())).build();
    }

}
