package com.lifestyle.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.lifestyle.domain.SurveyResults;
import com.lifestyle.domain.wrapper.SurveyResultWrapper;
import com.lifestyle.repository.AnswerRepository;
import com.lifestyle.repository.SurveyResultsRepository;
import com.lifestyle.service.SurveyResultsService;
import com.lifestyle.web.rest.util.HeaderUtil;
import com.lifestyle.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SurveyResults.
 */
@RestController
@RequestMapping("/api")
public class SurveyResultsResource {

    private final Logger log = LoggerFactory.getLogger(SurveyResultsResource.class);
        
    @Inject
    private SurveyResultsService surveyResultsService;
    
    @Inject
    private SurveyResultsRepository surveyResultsRepository;
    @Inject
    private AnswerRepository  answerRepository;
    
    /**
     * POST  /survey-results : Create a new surveyResults.
     *
     * @param surveyResults the surveyResults to create
     * @return the ResponseEntity with status 201 (Created) and with body the new surveyResults, or with status 400 (Bad Request) if the surveyResults has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/survey-results",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SurveyResults> createSurveyResults(@RequestBody SurveyResults surveyResults) throws URISyntaxException {
        log.debug("REST request to save SurveyResults : {}", surveyResults);
        if (surveyResults.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("surveyResults", "idexists", "A new surveyResults cannot already have an ID")).body(null);
        }
        SurveyResults result = surveyResultsService.save(surveyResults);
        return ResponseEntity.created(new URI("/api/survey-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("surveyResults", result.getId().toString()))
            .body(result);
    }
    
    /**
     * POST  /survey-results-list : Create a new List of surveyResults.
     *
     * @param surveyResults the surveyResults to create
     * @return the ResponseEntity with status 201 (Created) and with body the new surveyResults, or with status 400 (Bad Request) if the surveyResults has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/survey-results-list",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SurveyResults> createSurveyResultsList(@RequestBody List<SurveyResults> surveyResults) throws URISyntaxException {
        log.debug("REST request to save SurveyResultsList : {}", surveyResults);
        
        SurveyResults result = surveyResultsService.saveAll(surveyResults);
        return ResponseEntity.created(new URI("/api/survey-results/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("surveyResults", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /survey-results : Updates an existing surveyResults.
     *
     * @param surveyResults the surveyResults to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated surveyResults,
     * or with status 400 (Bad Request) if the surveyResults is not valid,
     * or with status 500 (Internal Server Error) if the surveyResults couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/survey-results",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SurveyResults> updateSurveyResults(@RequestBody SurveyResults surveyResults) throws URISyntaxException {
        log.debug("REST request to update SurveyResults : {}", surveyResults);
        if (surveyResults.getId() == null) {
            return createSurveyResults(surveyResults);
        }
        SurveyResults result = surveyResultsService.save(surveyResults);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("surveyResults", surveyResults.getId().toString()))
            .body(result);
    }

    /**
     * GET  /survey-results : get all the surveyResults.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of surveyResults in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/survey-results",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<SurveyResults>> getAllSurveyResults(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SurveyResults");
        Page<SurveyResults> page = surveyResultsService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/survey-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /survey-results/:id : get the "id" surveyResults.
     *
     * @param id the id of the surveyResults to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the surveyResults, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/survey-results/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SurveyResults> getSurveyResults(@PathVariable Long id) {
        log.debug("REST request to get SurveyResults : {}", id);
        SurveyResults surveyResults = surveyResultsService.findOne(id);
        return Optional.ofNullable(surveyResults)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /survey-results/:id : delete the "id" surveyResults.
     *
     * @param id the id of the surveyResults to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/survey-results/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSurveyResults(@PathVariable Long id) {
        log.debug("REST request to delete SurveyResults : {}", id);
        surveyResultsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("surveyResults", id.toString())).build();
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/survey-delete-all",
    		 method = RequestMethod.DELETE)
    public boolean deleteAllSurveyResults() {
    	surveyResultsRepository.deleteAll();
    	return true;
    }
    
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/survey-save-results",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        public boolean createSurveyResults(@RequestBody SurveyResultWrapper surveyResults) throws URISyntaxException {
            log.debug("REST request to save SurveyResults : {}", surveyResults);
            
            boolean result = surveyResultsService.createSurveyResults(surveyResults);
            
            // TODO: prepare response object and return
            
            return result;
        }
  
}
