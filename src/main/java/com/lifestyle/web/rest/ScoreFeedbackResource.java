package com.lifestyle.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.lifestyle.domain.ScoreFeedback;
import com.lifestyle.repository.ScoreFeedbackRepository;
import com.lifestyle.web.rest.util.HeaderUtil;
import com.lifestyle.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ScoreFeedback.
 */
@RestController
@RequestMapping("/api")
public class ScoreFeedbackResource {

    private final Logger log = LoggerFactory.getLogger(ScoreFeedbackResource.class);
        
    @Inject
    private ScoreFeedbackRepository scoreFeedbackRepository;
    
    /**
     * POST  /score-feedbacks : Create a new scoreFeedback.
     *
     * @param scoreFeedback the scoreFeedback to create
     * @return the ResponseEntity with status 201 (Created) and with body the new scoreFeedback, or with status 400 (Bad Request) if the scoreFeedback has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/score-feedbacks",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ScoreFeedback> createScoreFeedback(@Valid @RequestBody ScoreFeedback scoreFeedback) throws URISyntaxException {
        log.debug("REST request to save ScoreFeedback : {}", scoreFeedback);
        if (scoreFeedback.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("scoreFeedback", "idexists", "A new scoreFeedback cannot already have an ID")).body(null);
        }
        ScoreFeedback result = scoreFeedbackRepository.save(scoreFeedback);
        return ResponseEntity.created(new URI("/api/score-feedbacks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("scoreFeedback", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /score-feedbacks : Updates an existing scoreFeedback.
     *
     * @param scoreFeedback the scoreFeedback to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated scoreFeedback,
     * or with status 400 (Bad Request) if the scoreFeedback is not valid,
     * or with status 500 (Internal Server Error) if the scoreFeedback couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/score-feedbacks",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ScoreFeedback> updateScoreFeedback(@Valid @RequestBody ScoreFeedback scoreFeedback) throws URISyntaxException {
        log.debug("REST request to update ScoreFeedback : {}", scoreFeedback);
        if (scoreFeedback.getId() == null) {
            return createScoreFeedback(scoreFeedback);
        }
        ScoreFeedback result = scoreFeedbackRepository.save(scoreFeedback);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("scoreFeedback", scoreFeedback.getId().toString()))
            .body(result);
    }

    /**
     * GET  /score-feedbacks : get all the scoreFeedbacks.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of scoreFeedbacks in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/score-feedbacks",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ScoreFeedback>> getAllScoreFeedbacks(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ScoreFeedbacks");
        Page<ScoreFeedback> page = scoreFeedbackRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/score-feedbacks");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /score-feedbacks/:id : get the "id" scoreFeedback.
     *
     * @param id the id of the scoreFeedback to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the scoreFeedback, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/score-feedbacks/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ScoreFeedback> getScoreFeedback(@PathVariable Long id) {
        log.debug("REST request to get ScoreFeedback : {}", id);
        ScoreFeedback scoreFeedback = scoreFeedbackRepository.findOne(id);
        return Optional.ofNullable(scoreFeedback)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /score-feedbacks/:id : delete the "id" scoreFeedback.
     *
     * @param id the id of the scoreFeedback to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/score-feedbacks/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteScoreFeedback(@PathVariable Long id) {
        log.debug("REST request to delete ScoreFeedback : {}", id);
        scoreFeedbackRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("scoreFeedback", id.toString())).build();
    }

}
