package com.lifestyle.service.impl;

import com.lifestyle.service.SurveyResultsService;
import com.lifestyle.domain.Answer;
import com.lifestyle.domain.SurveyResults;
import com.lifestyle.domain.User;
import com.lifestyle.domain.wrapper.SurveyResultWrapper;
import com.lifestyle.repository.AnswerRepository;
import com.lifestyle.repository.QuestionRepository;
import com.lifestyle.repository.SurveyResultsRepository;
import com.lifestyle.repository.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing SurveyResults.
 */
@Service
@Transactional
public class SurveyResultsServiceImpl implements SurveyResultsService{

    private final Logger log = LoggerFactory.getLogger(SurveyResultsServiceImpl.class);
    
    @Inject
    private SurveyResultsRepository surveyResultsRepository;
    
    @Inject
    private AnswerRepository answerRepository;
    
    @Inject
    private UserRepository userRepository;
    
    @Inject
    private QuestionRepository questionRepository;
    
    /**
     * Save a surveyResults.
     * 
     * @param surveyResults the entity to save
     * @return the persisted entity
     */
    public SurveyResults save(SurveyResults surveyResults) {
        log.debug("Request to save SurveyResults : {}", surveyResults);
        SurveyResults result = surveyResultsRepository.save(surveyResults);
        return result;
    }
    
    /**
     * Save a list of surveyResults.
     * 
     * @param List surveyResults the entity to save
     * @return the persisted entity
     */
    public SurveyResults saveAll(List<SurveyResults> surveyResults) {
    	SurveyResults result = null;
    	for(int i = 0; i <= surveyResults.size(); i++) {
    		result = surveyResultsRepository.save(surveyResults.get(i));
    	}
    	
    	return result;
    }

    /**
     *  Get all the surveyResults.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<SurveyResults> findAll(Pageable pageable) {
        log.debug("Request to get all SurveyResults");
        Page<SurveyResults> result = surveyResultsRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one surveyResults by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public SurveyResults findOne(Long id) {
        log.debug("Request to get SurveyResults : {}", id);
        SurveyResults surveyResults = surveyResultsRepository.findOne(id);
        return surveyResults;
    }

    /**
     *  Delete the  surveyResults by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SurveyResults : {}", id);
        surveyResultsRepository.delete(id);
    }

    @Override
	public boolean createSurveyResults(SurveyResultWrapper surveyResults) {
		// Loop through the Answers and find the questions from the DB based on the ID and set it in Answers
		for(Answer answer: surveyResults.getAnswers()){
			answer.setQuestion(questionRepository.findOne(answer.getQuestion().getId()));
		}
		
		// Save the Answers. // TODO: Assumption only create
		
		List<Answer> answers = answerRepository.save(surveyResults.getAnswers());
		
        // Find the User if Already Exist in table 
        
        // If user is not existing then create new user
		String email = surveyResults.getUser().getEmail();
		Optional<User> existingUser = userRepository.findOneByEmail(email);
		if(  existingUser != null && existingUser.isPresent() && existingUser.get() != null) {
			surveyResults.setUser(existingUser.get());
		} 
		surveyResults.getUser().setPassword("43c68c2ca6945e2dfbdcc3429db00a43c68c2ca6945e2dfbdcc3429db00a");
		userRepository.save(surveyResults.getUser());
        
        // Prepare and Create the Survey Result
		List<SurveyResults> surveyResultsToSave = new ArrayList<SurveyResults>();
		for(Answer answer: surveyResults.getAnswers()){
			SurveyResults surveyResult = new SurveyResults();
			
			surveyResult.setAnswer_id(answer.getId());
			surveyResult.setCategory_id(answer.getQuestion().getCategory().getId());
			surveyResult.setQuestion_id(answer.getQuestion().getId());
			surveyResult.setUser_id(surveyResults.getUser().getId());
			//surveyResult.setSurvey_id(surveyResults.getSid());
			surveyResult.setFeedBackOne(surveyResults.getFeedback1score());
			surveyResult.setFeedBackTwo(surveyResults.getFeedback2score());
			
			surveyResultsToSave.add(surveyResult);
		}
		
		surveyResultsRepository.save(surveyResultsToSave);
		
		return false;
	}
}
