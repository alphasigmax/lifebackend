package com.lifestyle.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;

import com.lifestyle.domain.SurveyResults;
import com.lifestyle.domain.wrapper.SurveyResultWrapper;

/**
 * Service Interface for managing SurveyResults.
 */
public interface SurveyResultsService {

    /**
     * Save a surveyResults.
     * 
     * @param surveyResults the entity to save
     * @return the persisted entity
     */
    SurveyResults save(SurveyResults surveyResults);
    
    public SurveyResults saveAll(List<SurveyResults> surveyResults);

    /**
     *  Get all the surveyResults.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<SurveyResults> findAll(Pageable pageable);

    /**
     *  Get the "id" surveyResults.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    SurveyResults findOne(Long id);

    /**
     *  Delete the "id" surveyResults.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
    
    public boolean createSurveyResults(@RequestBody SurveyResultWrapper surveyResults) ;
}
