(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .controller('QuestionDetailController', QuestionDetailController);

    QuestionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Question', 'Category', 'QuestionType', 'Answer'];

    function QuestionDetailController($scope, $rootScope, $stateParams, entity, Question, Category, QuestionType, Answer) {
        var vm = this;
        vm.question = entity;
        vm.load = function (id) {
            Question.get({id: id}, function(result) {
                vm.question = result;
            });
        };
        var unsubscribe = $rootScope.$on('lifestylebackendApp:questionUpdate', function(event, result) {
            vm.question = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
