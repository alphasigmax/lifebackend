(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .controller('QuestionTypeDetailController', QuestionTypeDetailController);

    QuestionTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'QuestionType', 'Question'];

    function QuestionTypeDetailController($scope, $rootScope, $stateParams, entity, QuestionType, Question) {
        var vm = this;
        vm.questionType = entity;
        vm.load = function (id) {
            QuestionType.get({id: id}, function(result) {
                vm.questionType = result;
            });
        };
        var unsubscribe = $rootScope.$on('lifestylebackendApp:questionTypeUpdate', function(event, result) {
            vm.questionType = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
