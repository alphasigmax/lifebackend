(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('question-type', {
            parent: 'entity',
            url: '/question-type?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'lifestylebackendApp.questionType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/question-type/question-types.html',
                    controller: 'QuestionTypeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('questionType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('question-type-detail', {
            parent: 'entity',
            url: '/question-type/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'lifestylebackendApp.questionType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/question-type/question-type-detail.html',
                    controller: 'QuestionTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('questionType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'QuestionType', function($stateParams, QuestionType) {
                    return QuestionType.get({id : $stateParams.id});
                }]
            }
        })
        .state('question-type.new', {
            parent: 'question-type',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/question-type/question-type-dialog.html',
                    controller: 'QuestionTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                questionType: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('question-type', null, { reload: true });
                }, function() {
                    $state.go('question-type');
                });
            }]
        })
        .state('question-type.edit', {
            parent: 'question-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/question-type/question-type-dialog.html',
                    controller: 'QuestionTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['QuestionType', function(QuestionType) {
                            return QuestionType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('question-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('question-type.delete', {
            parent: 'question-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/question-type/question-type-delete-dialog.html',
                    controller: 'QuestionTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['QuestionType', function(QuestionType) {
                            return QuestionType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('question-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
