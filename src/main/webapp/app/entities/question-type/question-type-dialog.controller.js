(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .controller('QuestionTypeDialogController', QuestionTypeDialogController);

    QuestionTypeDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'QuestionType', 'Question'];

    function QuestionTypeDialogController ($scope, $stateParams, $uibModalInstance, entity, QuestionType, Question) {
        var vm = this;
        vm.questionType = entity;
        vm.questions = Question.query();
        vm.load = function(id) {
            QuestionType.get({id : id}, function(result) {
                vm.questionType = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lifestylebackendApp:questionTypeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.questionType.id !== null) {
                QuestionType.update(vm.questionType, onSaveSuccess, onSaveError);
            } else {
                QuestionType.save(vm.questionType, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
