(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .controller('QuestionTypeDeleteController',QuestionTypeDeleteController);

    QuestionTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'QuestionType'];

    function QuestionTypeDeleteController($uibModalInstance, entity, QuestionType) {
        var vm = this;
        vm.questionType = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            QuestionType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
