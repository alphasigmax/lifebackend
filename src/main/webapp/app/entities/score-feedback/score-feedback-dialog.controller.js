(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .controller('ScoreFeedbackDialogController', ScoreFeedbackDialogController);

    ScoreFeedbackDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'ScoreFeedback', 'Category'];

    function ScoreFeedbackDialogController ($scope, $stateParams, $uibModalInstance, $q, entity, ScoreFeedback, Category) {
        var vm = this;
        vm.scoreFeedback = entity;
        vm.categorys = Category.query({filter: 'scorefeedback-is-null'});
        $q.all([vm.scoreFeedback.$promise, vm.categorys.$promise]).then(function() {
            if (!vm.scoreFeedback.category || !vm.scoreFeedback.category.id) {
                return $q.reject();
            }
            return Category.get({id : vm.scoreFeedback.category.id}).$promise;
        }).then(function(category) {
            vm.categorys.push(category);
        });
        vm.load = function(id) {
            ScoreFeedback.get({id : id}, function(result) {
                vm.scoreFeedback = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lifestylebackendApp:scoreFeedbackUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.scoreFeedback.id !== null) {
                ScoreFeedback.update(vm.scoreFeedback, onSaveSuccess, onSaveError);
            } else {
                ScoreFeedback.save(vm.scoreFeedback, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
