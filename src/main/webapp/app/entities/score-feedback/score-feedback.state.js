(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('score-feedback', {
            parent: 'entity',
            url: '/score-feedback?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'lifestylebackendApp.scoreFeedback.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/score-feedback/score-feedbacks.html',
                    controller: 'ScoreFeedbackController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('scoreFeedback');
                    $translatePartialLoader.addPart('scorelevel');
                    $translatePartialLoader.addPart('scorelevel');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('score-feedback-detail', {
            parent: 'entity',
            url: '/score-feedback/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'lifestylebackendApp.scoreFeedback.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/score-feedback/score-feedback-detail.html',
                    controller: 'ScoreFeedbackDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('scoreFeedback');
                    $translatePartialLoader.addPart('scorelevel');
                    $translatePartialLoader.addPart('scorelevel');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ScoreFeedback', function($stateParams, ScoreFeedback) {
                    return ScoreFeedback.get({id : $stateParams.id});
                }]
            }
        })
        .state('score-feedback.new', {
            parent: 'score-feedback',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/score-feedback/score-feedback-dialog.html',
                    controller: 'ScoreFeedbackDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                scoreLevelOne: null,
                                scoreLevelTwo: null,
                                userFeedBack: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('score-feedback', null, { reload: true });
                }, function() {
                    $state.go('score-feedback');
                });
            }]
        })
        .state('score-feedback.edit', {
            parent: 'score-feedback',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/score-feedback/score-feedback-dialog.html',
                    controller: 'ScoreFeedbackDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ScoreFeedback', function(ScoreFeedback) {
                            return ScoreFeedback.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('score-feedback', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('score-feedback.delete', {
            parent: 'score-feedback',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/score-feedback/score-feedback-delete-dialog.html',
                    controller: 'ScoreFeedbackDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ScoreFeedback', function(ScoreFeedback) {
                            return ScoreFeedback.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('score-feedback', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
