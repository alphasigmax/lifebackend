(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .controller('ScoreFeedbackDeleteController',ScoreFeedbackDeleteController);

    ScoreFeedbackDeleteController.$inject = ['$uibModalInstance', 'entity', 'ScoreFeedback'];

    function ScoreFeedbackDeleteController($uibModalInstance, entity, ScoreFeedback) {
        var vm = this;
        vm.scoreFeedback = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            ScoreFeedback.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
