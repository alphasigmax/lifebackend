(function() {
    'use strict';
    angular
        .module('lifestylebackendApp')
        .factory('ScoreFeedback', ScoreFeedback);

    ScoreFeedback.$inject = ['$resource'];

    function ScoreFeedback ($resource) {
        var resourceUrl =  'api/score-feedbacks/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
