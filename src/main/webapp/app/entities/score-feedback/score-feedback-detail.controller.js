(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .controller('ScoreFeedbackDetailController', ScoreFeedbackDetailController);

    ScoreFeedbackDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ScoreFeedback', 'Category'];

    function ScoreFeedbackDetailController($scope, $rootScope, $stateParams, entity, ScoreFeedback, Category) {
        var vm = this;
        vm.scoreFeedback = entity;
        vm.load = function (id) {
            ScoreFeedback.get({id: id}, function(result) {
                vm.scoreFeedback = result;
            });
        };
        var unsubscribe = $rootScope.$on('lifestylebackendApp:scoreFeedbackUpdate', function(event, result) {
            vm.scoreFeedback = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
