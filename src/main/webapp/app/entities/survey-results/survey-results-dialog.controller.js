(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .controller('SurveyResultsDialogController', SurveyResultsDialogController);

    SurveyResultsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SurveyResults'];

    function SurveyResultsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SurveyResults) {
        var vm = this;
        vm.surveyResults = entity;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        var onSaveSuccess = function (result) {
            $scope.$emit('lifestylebackendApp:surveyResultsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.surveyResults.id !== null) {
                SurveyResults.update(vm.surveyResults, onSaveSuccess, onSaveError);
            } else {
                SurveyResults.save(vm.surveyResults, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
