(function() {
    'use strict';
    angular
        .module('lifestylebackendApp')
        .factory('SurveyResults', SurveyResults);

    SurveyResults.$inject = ['$resource'];

    function SurveyResults ($resource) {
        var resourceUrl =  'api/survey-results/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
