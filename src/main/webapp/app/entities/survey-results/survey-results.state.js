(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('survey-results', {
            parent: 'entity',
            url: '/survey-results',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'lifestylebackendApp.surveyResults.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/survey-results/survey-results.html',
                    controller: 'SurveyResultsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('surveyResults');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('survey-results-detail', {
            parent: 'entity',
            url: '/survey-results/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'lifestylebackendApp.surveyResults.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/survey-results/survey-results-detail.html',
                    controller: 'SurveyResultsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('surveyResults');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SurveyResults', function($stateParams, SurveyResults) {
                    return SurveyResults.get({id : $stateParams.id});
                }]
            }
        })
        .state('survey-results.new', {
            parent: 'survey-results',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/survey-results/survey-results-dialog.html',
                    controller: 'SurveyResultsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                user_id: null,
                                answer_id: null,
                                question_id: null,
                                category_id: null,
                                survey_id: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('survey-results', null, { reload: true });
                }, function() {
                    $state.go('survey-results');
                });
            }]
        })
        .state('survey-results.edit', {
            parent: 'survey-results',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/survey-results/survey-results-dialog.html',
                    controller: 'SurveyResultsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SurveyResults', function(SurveyResults) {
                            return SurveyResults.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('survey-results', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('survey-results.delete', {
            parent: 'survey-results',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/survey-results/survey-results-delete-dialog.html',
                    controller: 'SurveyResultsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SurveyResults', function(SurveyResults) {
                            return SurveyResults.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('survey-results', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
