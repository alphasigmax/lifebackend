(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .controller('SurveyResultsDeleteController',SurveyResultsDeleteController);

    SurveyResultsDeleteController.$inject = ['$uibModalInstance', 'entity', 'SurveyResults'];

    function SurveyResultsDeleteController($uibModalInstance, entity, SurveyResults) {
        var vm = this;
        vm.surveyResults = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            SurveyResults.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
