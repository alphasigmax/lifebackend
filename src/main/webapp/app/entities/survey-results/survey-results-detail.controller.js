(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .controller('SurveyResultsDetailController', SurveyResultsDetailController);

    SurveyResultsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'SurveyResults'];

    function SurveyResultsDetailController($scope, $rootScope, $stateParams, entity, SurveyResults) {
        var vm = this;
        vm.surveyResults = entity;
        
        var unsubscribe = $rootScope.$on('lifestylebackendApp:surveyResultsUpdate', function(event, result) {
            vm.surveyResults = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
