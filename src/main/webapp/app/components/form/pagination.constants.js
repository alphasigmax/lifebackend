(function() {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
