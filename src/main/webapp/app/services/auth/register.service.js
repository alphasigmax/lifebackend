(function () {
    'use strict';

    angular
        .module('lifestylebackendApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
