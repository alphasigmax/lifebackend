 'use strict';

angular.module('lifestylebackendApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-lifestylebackendApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-lifestylebackendApp-params')});
                }
                return response;
            }
        };
    });
