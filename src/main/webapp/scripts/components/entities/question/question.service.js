'use strict';

angular.module('lifestylebackendApp')
    .factory('Question', function ($resource, DateUtils) {
        return $resource('api/questions/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
