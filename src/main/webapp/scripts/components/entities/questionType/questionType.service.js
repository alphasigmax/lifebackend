'use strict';

angular.module('lifestylebackendApp')
    .factory('QuestionType', function ($resource, DateUtils) {
        return $resource('api/questionTypes/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
