'use strict';

angular.module('lifestylebackendApp')
    .controller('CategoryDetailController', function ($scope, $rootScope, $stateParams, entity, Category, Question) {
        $scope.category = entity;
        $scope.load = function (id) {
            Category.get({id: id}, function(result) {
                $scope.category = result;
            });
        };
        var unsubscribe = $rootScope.$on('lifestylebackendApp:categoryUpdate', function(event, result) {
            $scope.category = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
