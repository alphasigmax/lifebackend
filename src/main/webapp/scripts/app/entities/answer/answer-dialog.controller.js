'use strict';

angular.module('lifestylebackendApp').controller('AnswerDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Answer', 'Question',
        function($scope, $stateParams, $uibModalInstance, entity, Answer, Question) {

        $scope.answer = entity;
        $scope.questions = Question.query();
        $scope.load = function(id) {
            Answer.get({id : id}, function(result) {
                $scope.answer = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lifestylebackendApp:answerUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.answer.id != null) {
                Answer.update($scope.answer, onSaveSuccess, onSaveError);
            } else {
                Answer.save($scope.answer, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
