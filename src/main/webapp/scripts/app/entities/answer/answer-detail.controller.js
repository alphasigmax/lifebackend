'use strict';

angular.module('lifestylebackendApp')
    .controller('AnswerDetailController', function ($scope, $rootScope, $stateParams, entity, Answer, Question) {
        $scope.answer = entity;
        $scope.load = function (id) {
            Answer.get({id: id}, function(result) {
                $scope.answer = result;
            });
        };
        var unsubscribe = $rootScope.$on('lifestylebackendApp:answerUpdate', function(event, result) {
            $scope.answer = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
