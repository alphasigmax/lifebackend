'use strict';

angular.module('lifestylebackendApp').controller('QuestionDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Question', 'Category', 'QuestionType', 'Answer',
        function($scope, $stateParams, $uibModalInstance, entity, Question, Category, QuestionType, Answer) {

        $scope.question = entity;
        $scope.categorys = Category.query();
        $scope.questiontypes = QuestionType.query();
        $scope.answers = Answer.query();
        $scope.load = function(id) {
            Question.get({id : id}, function(result) {
                $scope.question = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lifestylebackendApp:questionUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.question.id != null) {
                Question.update($scope.question, onSaveSuccess, onSaveError);
            } else {
                Question.save($scope.question, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
