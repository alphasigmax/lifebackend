'use strict';

angular.module('lifestylebackendApp')
    .controller('QuestionDetailController', function ($scope, $rootScope, $stateParams, entity, Question, Category, QuestionType, Answer) {
        $scope.question = entity;
        $scope.load = function (id) {
            Question.get({id: id}, function(result) {
                $scope.question = result;
            });
        };
        var unsubscribe = $rootScope.$on('lifestylebackendApp:questionUpdate', function(event, result) {
            $scope.question = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
