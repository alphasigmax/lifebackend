'use strict';

angular.module('lifestylebackendApp')
	.controller('QuestionTypeDeleteController', function($scope, $uibModalInstance, entity, QuestionType) {

        $scope.questionType = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            QuestionType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
