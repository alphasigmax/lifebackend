'use strict';

angular.module('lifestylebackendApp').controller('QuestionTypeDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'QuestionType', 'Question',
        function($scope, $stateParams, $uibModalInstance, entity, QuestionType, Question) {

        $scope.questionType = entity;
        $scope.questions = Question.query();
        $scope.load = function(id) {
            QuestionType.get({id : id}, function(result) {
                $scope.questionType = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lifestylebackendApp:questionTypeUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.questionType.id != null) {
                QuestionType.update($scope.questionType, onSaveSuccess, onSaveError);
            } else {
                QuestionType.save($scope.questionType, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
