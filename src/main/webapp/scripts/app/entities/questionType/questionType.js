'use strict';

angular.module('lifestylebackendApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('questionType', {
                parent: 'entity',
                url: '/questionTypes',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'lifestylebackendApp.questionType.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/questionType/questionTypes.html',
                        controller: 'QuestionTypeController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('questionType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('questionType.detail', {
                parent: 'entity',
                url: '/questionType/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'lifestylebackendApp.questionType.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/questionType/questionType-detail.html',
                        controller: 'QuestionTypeDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('questionType');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'QuestionType', function($stateParams, QuestionType) {
                        return QuestionType.get({id : $stateParams.id});
                    }]
                }
            })
            .state('questionType.new', {
                parent: 'questionType',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/questionType/questionType-dialog.html',
                        controller: 'QuestionTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    questionType: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('questionType', null, { reload: true });
                    }, function() {
                        $state.go('questionType');
                    })
                }]
            })
            .state('questionType.edit', {
                parent: 'questionType',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/questionType/questionType-dialog.html',
                        controller: 'QuestionTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['QuestionType', function(QuestionType) {
                                return QuestionType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('questionType', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('questionType.delete', {
                parent: 'questionType',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/questionType/questionType-delete-dialog.html',
                        controller: 'QuestionTypeDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['QuestionType', function(QuestionType) {
                                return QuestionType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('questionType', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
