'use strict';

angular.module('lifestylebackendApp')
    .controller('QuestionTypeDetailController', function ($scope, $rootScope, $stateParams, entity, QuestionType, Question) {
        $scope.questionType = entity;
        $scope.load = function (id) {
            QuestionType.get({id: id}, function(result) {
                $scope.questionType = result;
            });
        };
        var unsubscribe = $rootScope.$on('lifestylebackendApp:questionTypeUpdate', function(event, result) {
            $scope.questionType = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
