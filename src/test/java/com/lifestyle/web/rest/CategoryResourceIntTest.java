package com.lifestyle.web.rest;

import com.lifestyle.LifestylebackendApp;
import com.lifestyle.domain.Category;
import com.lifestyle.repository.CategoryRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CategoryResource REST controller.
 *
 * @see CategoryResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LifestylebackendApp.class)
@WebAppConfiguration
@IntegrationTest
public class CategoryResourceIntTest {

    private static final String DEFAULT_CATEGORY_NAME = "AAAAA";
    private static final String UPDATED_CATEGORY_NAME = "BBBBB";

    private static final Integer DEFAULT_NUMBER_OF_QUESTIONS_PER_PAGE = 1;
    private static final Integer UPDATED_NUMBER_OF_QUESTIONS_PER_PAGE = 2;
    private static final String DEFAULT_QUESTION_HEADING = "AAAAA";
    private static final String UPDATED_QUESTION_HEADING = "BBBBB";

    private static final Integer DEFAULT_CATEGORY_GROUP_ID = 1;
    private static final Integer UPDATED_CATEGORY_GROUP_ID = 2;
    private static final String DEFAULT_PRE_TEXT = "AAAAA";
    private static final String UPDATED_PRE_TEXT = "BBBBB";
    private static final String DEFAULT_POST_TEXT = "AAAAA";
    private static final String UPDATED_POST_TEXT = "BBBBB";

    @Inject
    private CategoryRepository categoryRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCategoryMockMvc;

    private Category category;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CategoryResource categoryResource = new CategoryResource();
        ReflectionTestUtils.setField(categoryResource, "categoryRepository", categoryRepository);
        this.restCategoryMockMvc = MockMvcBuilders.standaloneSetup(categoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        category = new Category();
        category.setCategoryName(DEFAULT_CATEGORY_NAME);
        category.setNumberOfQuestionsPerPage(DEFAULT_NUMBER_OF_QUESTIONS_PER_PAGE);
        category.setQuestionHeading(DEFAULT_QUESTION_HEADING);
        category.setCategoryGroupId(DEFAULT_CATEGORY_GROUP_ID);
        category.setPreText(DEFAULT_PRE_TEXT);
        category.setPostText(DEFAULT_POST_TEXT);
    }

    @Test
    @Transactional
    public void createCategory() throws Exception {
        int databaseSizeBeforeCreate = categoryRepository.findAll().size();

        // Create the Category

        restCategoryMockMvc.perform(post("/api/categories")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(category)))
                .andExpect(status().isCreated());

        // Validate the Category in the database
        List<Category> categories = categoryRepository.findAll();
        assertThat(categories).hasSize(databaseSizeBeforeCreate + 1);
        Category testCategory = categories.get(categories.size() - 1);
        assertThat(testCategory.getCategoryName()).isEqualTo(DEFAULT_CATEGORY_NAME);
        assertThat(testCategory.getNumberOfQuestionsPerPage()).isEqualTo(DEFAULT_NUMBER_OF_QUESTIONS_PER_PAGE);
        assertThat(testCategory.getQuestionHeading()).isEqualTo(DEFAULT_QUESTION_HEADING);
        assertThat(testCategory.getCategoryGroupId()).isEqualTo(DEFAULT_CATEGORY_GROUP_ID);
        assertThat(testCategory.getPreText()).isEqualTo(DEFAULT_PRE_TEXT);
        assertThat(testCategory.getPostText()).isEqualTo(DEFAULT_POST_TEXT);
    }

    @Test
    @Transactional
    public void getAllCategories() throws Exception {
        // Initialize the database
        categoryRepository.saveAndFlush(category);

        // Get all the categories
        restCategoryMockMvc.perform(get("/api/categories?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(category.getId().intValue())))
                .andExpect(jsonPath("$.[*].categoryName").value(hasItem(DEFAULT_CATEGORY_NAME.toString())))
                .andExpect(jsonPath("$.[*].numberOfQuestionsPerPage").value(hasItem(DEFAULT_NUMBER_OF_QUESTIONS_PER_PAGE)))
                .andExpect(jsonPath("$.[*].questionHeading").value(hasItem(DEFAULT_QUESTION_HEADING.toString())))
                .andExpect(jsonPath("$.[*].categoryGroupId").value(hasItem(DEFAULT_CATEGORY_GROUP_ID)))
                .andExpect(jsonPath("$.[*].preText").value(hasItem(DEFAULT_PRE_TEXT.toString())))
                .andExpect(jsonPath("$.[*].postText").value(hasItem(DEFAULT_POST_TEXT.toString())));
    }

    @Test
    @Transactional
    public void getCategory() throws Exception {
        // Initialize the database
        categoryRepository.saveAndFlush(category);

        // Get the category
        restCategoryMockMvc.perform(get("/api/categories/{id}", category.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(category.getId().intValue()))
            .andExpect(jsonPath("$.categoryName").value(DEFAULT_CATEGORY_NAME.toString()))
            .andExpect(jsonPath("$.numberOfQuestionsPerPage").value(DEFAULT_NUMBER_OF_QUESTIONS_PER_PAGE))
            .andExpect(jsonPath("$.questionHeading").value(DEFAULT_QUESTION_HEADING.toString()))
            .andExpect(jsonPath("$.categoryGroupId").value(DEFAULT_CATEGORY_GROUP_ID))
            .andExpect(jsonPath("$.preText").value(DEFAULT_PRE_TEXT.toString()))
            .andExpect(jsonPath("$.postText").value(DEFAULT_POST_TEXT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCategory() throws Exception {
        // Get the category
        restCategoryMockMvc.perform(get("/api/categories/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategory() throws Exception {
        // Initialize the database
        categoryRepository.saveAndFlush(category);
        int databaseSizeBeforeUpdate = categoryRepository.findAll().size();

        // Update the category
        Category updatedCategory = new Category();
        updatedCategory.setId(category.getId());
        updatedCategory.setCategoryName(UPDATED_CATEGORY_NAME);
        updatedCategory.setNumberOfQuestionsPerPage(UPDATED_NUMBER_OF_QUESTIONS_PER_PAGE);
        updatedCategory.setQuestionHeading(UPDATED_QUESTION_HEADING);
        updatedCategory.setCategoryGroupId(UPDATED_CATEGORY_GROUP_ID);
        updatedCategory.setPreText(UPDATED_PRE_TEXT);
        updatedCategory.setPostText(UPDATED_POST_TEXT);

        restCategoryMockMvc.perform(put("/api/categories")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedCategory)))
                .andExpect(status().isOk());

        // Validate the Category in the database
        List<Category> categories = categoryRepository.findAll();
        assertThat(categories).hasSize(databaseSizeBeforeUpdate);
        Category testCategory = categories.get(categories.size() - 1);
        assertThat(testCategory.getCategoryName()).isEqualTo(UPDATED_CATEGORY_NAME);
        assertThat(testCategory.getNumberOfQuestionsPerPage()).isEqualTo(UPDATED_NUMBER_OF_QUESTIONS_PER_PAGE);
        assertThat(testCategory.getQuestionHeading()).isEqualTo(UPDATED_QUESTION_HEADING);
        assertThat(testCategory.getCategoryGroupId()).isEqualTo(UPDATED_CATEGORY_GROUP_ID);
        assertThat(testCategory.getPreText()).isEqualTo(UPDATED_PRE_TEXT);
        assertThat(testCategory.getPostText()).isEqualTo(UPDATED_POST_TEXT);
    }

    @Test
    @Transactional
    public void deleteCategory() throws Exception {
        // Initialize the database
        categoryRepository.saveAndFlush(category);
        int databaseSizeBeforeDelete = categoryRepository.findAll().size();

        // Get the category
        restCategoryMockMvc.perform(delete("/api/categories/{id}", category.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Category> categories = categoryRepository.findAll();
        assertThat(categories).hasSize(databaseSizeBeforeDelete - 1);
    }
}
