package com.lifestyle.web.rest;

import com.lifestyle.LifestylebackendApp;
import com.lifestyle.domain.SurveyResults;
import com.lifestyle.repository.SurveyResultsRepository;
import com.lifestyle.service.SurveyResultsService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SurveyResultsResource REST controller.
 *
 * @see SurveyResultsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LifestylebackendApp.class)
@WebAppConfiguration
@IntegrationTest
public class SurveyResultsResourceIntTest {


    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;

    private static final Long DEFAULT_ANSWER_ID = 1L;
    private static final Long UPDATED_ANSWER_ID = 2L;

    private static final Long DEFAULT_QUESTION_ID = 1L;
    private static final Long UPDATED_QUESTION_ID = 2L;

    private static final Long DEFAULT_CATEGORY_ID = 1L;
    private static final Long UPDATED_CATEGORY_ID = 2L;

    private static final Long DEFAULT_SURVEY_ID = 1L;
    private static final Long UPDATED_SURVEY_ID = 2L;

    @Inject
    private SurveyResultsRepository surveyResultsRepository;

    @Inject
    private SurveyResultsService surveyResultsService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSurveyResultsMockMvc;

    private SurveyResults surveyResults;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SurveyResultsResource surveyResultsResource = new SurveyResultsResource();
        ReflectionTestUtils.setField(surveyResultsResource, "surveyResultsService", surveyResultsService);
        this.restSurveyResultsMockMvc = MockMvcBuilders.standaloneSetup(surveyResultsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        surveyResults = new SurveyResults();
        surveyResults.setUser_id(DEFAULT_USER_ID);
        surveyResults.setAnswer_id(DEFAULT_ANSWER_ID);
        surveyResults.setQuestion_id(DEFAULT_QUESTION_ID);
        surveyResults.setCategory_id(DEFAULT_CATEGORY_ID);
        surveyResults.setSurvey_id(DEFAULT_SURVEY_ID);
    }

    @Test
    @Transactional
    public void createSurveyResults() throws Exception {
        int databaseSizeBeforeCreate = surveyResultsRepository.findAll().size();

        // Create the SurveyResults

        restSurveyResultsMockMvc.perform(post("/api/survey-results")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(surveyResults)))
                .andExpect(status().isCreated());

        // Validate the SurveyResults in the database
        List<SurveyResults> surveyResults = surveyResultsRepository.findAll();
        assertThat(surveyResults).hasSize(databaseSizeBeforeCreate + 1);
        SurveyResults testSurveyResults = surveyResults.get(surveyResults.size() - 1);
        assertThat(testSurveyResults.getUser_id()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testSurveyResults.getAnswer_id()).isEqualTo(DEFAULT_ANSWER_ID);
        assertThat(testSurveyResults.getQuestion_id()).isEqualTo(DEFAULT_QUESTION_ID);
        assertThat(testSurveyResults.getCategory_id()).isEqualTo(DEFAULT_CATEGORY_ID);
        assertThat(testSurveyResults.getSurvey_id()).isEqualTo(DEFAULT_SURVEY_ID);
    }

    @Test
    @Transactional
    public void getAllSurveyResults() throws Exception {
        // Initialize the database
        surveyResultsRepository.saveAndFlush(surveyResults);

        // Get all the surveyResults
        restSurveyResultsMockMvc.perform(get("/api/survey-results?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(surveyResults.getId().intValue())))
                .andExpect(jsonPath("$.[*].user_id").value(hasItem(DEFAULT_USER_ID.intValue())))
                .andExpect(jsonPath("$.[*].answer_id").value(hasItem(DEFAULT_ANSWER_ID.intValue())))
                .andExpect(jsonPath("$.[*].question_id").value(hasItem(DEFAULT_QUESTION_ID.intValue())))
                .andExpect(jsonPath("$.[*].category_id").value(hasItem(DEFAULT_CATEGORY_ID.intValue())))
                .andExpect(jsonPath("$.[*].survey_id").value(hasItem(DEFAULT_SURVEY_ID.intValue())));
    }

    @Test
    @Transactional
    public void getSurveyResults() throws Exception {
        // Initialize the database
        surveyResultsRepository.saveAndFlush(surveyResults);

        // Get the surveyResults
        restSurveyResultsMockMvc.perform(get("/api/survey-results/{id}", surveyResults.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(surveyResults.getId().intValue()))
            .andExpect(jsonPath("$.user_id").value(DEFAULT_USER_ID.intValue()))
            .andExpect(jsonPath("$.answer_id").value(DEFAULT_ANSWER_ID.intValue()))
            .andExpect(jsonPath("$.question_id").value(DEFAULT_QUESTION_ID.intValue()))
            .andExpect(jsonPath("$.category_id").value(DEFAULT_CATEGORY_ID.intValue()))
            .andExpect(jsonPath("$.survey_id").value(DEFAULT_SURVEY_ID.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSurveyResults() throws Exception {
        // Get the surveyResults
        restSurveyResultsMockMvc.perform(get("/api/survey-results/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSurveyResults() throws Exception {
        // Initialize the database
        surveyResultsService.save(surveyResults);

        int databaseSizeBeforeUpdate = surveyResultsRepository.findAll().size();

        // Update the surveyResults
        SurveyResults updatedSurveyResults = new SurveyResults();
        updatedSurveyResults.setId(surveyResults.getId());
        updatedSurveyResults.setUser_id(UPDATED_USER_ID);
        updatedSurveyResults.setAnswer_id(UPDATED_ANSWER_ID);
        updatedSurveyResults.setQuestion_id(UPDATED_QUESTION_ID);
        updatedSurveyResults.setCategory_id(UPDATED_CATEGORY_ID);
        updatedSurveyResults.setSurvey_id(UPDATED_SURVEY_ID);

        restSurveyResultsMockMvc.perform(put("/api/survey-results")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedSurveyResults)))
                .andExpect(status().isOk());

        // Validate the SurveyResults in the database
        List<SurveyResults> surveyResults = surveyResultsRepository.findAll();
        assertThat(surveyResults).hasSize(databaseSizeBeforeUpdate);
        SurveyResults testSurveyResults = surveyResults.get(surveyResults.size() - 1);
        assertThat(testSurveyResults.getUser_id()).isEqualTo(UPDATED_USER_ID);
        assertThat(testSurveyResults.getAnswer_id()).isEqualTo(UPDATED_ANSWER_ID);
        assertThat(testSurveyResults.getQuestion_id()).isEqualTo(UPDATED_QUESTION_ID);
        assertThat(testSurveyResults.getCategory_id()).isEqualTo(UPDATED_CATEGORY_ID);
        assertThat(testSurveyResults.getSurvey_id()).isEqualTo(UPDATED_SURVEY_ID);
    }

    @Test
    @Transactional
    public void deleteSurveyResults() throws Exception {
        // Initialize the database
        surveyResultsService.save(surveyResults);

        int databaseSizeBeforeDelete = surveyResultsRepository.findAll().size();

        // Get the surveyResults
        restSurveyResultsMockMvc.perform(delete("/api/survey-results/{id}", surveyResults.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<SurveyResults> surveyResults = surveyResultsRepository.findAll();
        assertThat(surveyResults).hasSize(databaseSizeBeforeDelete - 1);
    }
}
