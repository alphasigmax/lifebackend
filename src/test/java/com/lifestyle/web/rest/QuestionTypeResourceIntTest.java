package com.lifestyle.web.rest;

import com.lifestyle.LifestylebackendApp;
import com.lifestyle.domain.QuestionType;
import com.lifestyle.repository.QuestionTypeRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the QuestionTypeResource REST controller.
 *
 * @see QuestionTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LifestylebackendApp.class)
@WebAppConfiguration
@IntegrationTest
public class QuestionTypeResourceIntTest {

    private static final String DEFAULT_QUESTION_TYPE = "AAAAA";
    private static final String UPDATED_QUESTION_TYPE = "BBBBB";

    @Inject
    private QuestionTypeRepository questionTypeRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restQuestionTypeMockMvc;

    private QuestionType questionType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        QuestionTypeResource questionTypeResource = new QuestionTypeResource();
        ReflectionTestUtils.setField(questionTypeResource, "questionTypeRepository", questionTypeRepository);
        this.restQuestionTypeMockMvc = MockMvcBuilders.standaloneSetup(questionTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        questionType = new QuestionType();
        questionType.setQuestionType(DEFAULT_QUESTION_TYPE);
    }

    @Test
    @Transactional
    public void createQuestionType() throws Exception {
        int databaseSizeBeforeCreate = questionTypeRepository.findAll().size();

        // Create the QuestionType

        restQuestionTypeMockMvc.perform(post("/api/question-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(questionType)))
                .andExpect(status().isCreated());

        // Validate the QuestionType in the database
        List<QuestionType> questionTypes = questionTypeRepository.findAll();
        assertThat(questionTypes).hasSize(databaseSizeBeforeCreate + 1);
        QuestionType testQuestionType = questionTypes.get(questionTypes.size() - 1);
        assertThat(testQuestionType.getQuestionType()).isEqualTo(DEFAULT_QUESTION_TYPE);
    }

    @Test
    @Transactional
    public void getAllQuestionTypes() throws Exception {
        // Initialize the database
        questionTypeRepository.saveAndFlush(questionType);

        // Get all the questionTypes
        restQuestionTypeMockMvc.perform(get("/api/question-types?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(questionType.getId().intValue())))
                .andExpect(jsonPath("$.[*].questionType").value(hasItem(DEFAULT_QUESTION_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getQuestionType() throws Exception {
        // Initialize the database
        questionTypeRepository.saveAndFlush(questionType);

        // Get the questionType
        restQuestionTypeMockMvc.perform(get("/api/question-types/{id}", questionType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(questionType.getId().intValue()))
            .andExpect(jsonPath("$.questionType").value(DEFAULT_QUESTION_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingQuestionType() throws Exception {
        // Get the questionType
        restQuestionTypeMockMvc.perform(get("/api/question-types/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQuestionType() throws Exception {
        // Initialize the database
        questionTypeRepository.saveAndFlush(questionType);
        int databaseSizeBeforeUpdate = questionTypeRepository.findAll().size();

        // Update the questionType
        QuestionType updatedQuestionType = new QuestionType();
        updatedQuestionType.setId(questionType.getId());
        updatedQuestionType.setQuestionType(UPDATED_QUESTION_TYPE);

        restQuestionTypeMockMvc.perform(put("/api/question-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedQuestionType)))
                .andExpect(status().isOk());

        // Validate the QuestionType in the database
        List<QuestionType> questionTypes = questionTypeRepository.findAll();
        assertThat(questionTypes).hasSize(databaseSizeBeforeUpdate);
        QuestionType testQuestionType = questionTypes.get(questionTypes.size() - 1);
        assertThat(testQuestionType.getQuestionType()).isEqualTo(UPDATED_QUESTION_TYPE);
    }

    @Test
    @Transactional
    public void deleteQuestionType() throws Exception {
        // Initialize the database
        questionTypeRepository.saveAndFlush(questionType);
        int databaseSizeBeforeDelete = questionTypeRepository.findAll().size();

        // Get the questionType
        restQuestionTypeMockMvc.perform(delete("/api/question-types/{id}", questionType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<QuestionType> questionTypes = questionTypeRepository.findAll();
        assertThat(questionTypes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
