'use strict';

describe('Controller Tests', function() {

    describe('ScoreFeedback Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockScoreFeedback, MockCategory;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockScoreFeedback = jasmine.createSpy('MockScoreFeedback');
            MockCategory = jasmine.createSpy('MockCategory');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'ScoreFeedback': MockScoreFeedback,
                'Category': MockCategory
            };
            createController = function() {
                $injector.get('$controller')("ScoreFeedbackDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'lifestylebackendApp:scoreFeedbackUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
