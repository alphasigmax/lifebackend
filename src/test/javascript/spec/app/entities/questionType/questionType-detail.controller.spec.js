'use strict';

describe('Controller Tests', function() {

    describe('QuestionType Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockQuestionType, MockQuestion;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockQuestionType = jasmine.createSpy('MockQuestionType');
            MockQuestion = jasmine.createSpy('MockQuestion');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'QuestionType': MockQuestionType,
                'Question': MockQuestion
            };
            createController = function() {
                $injector.get('$controller')("QuestionTypeDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'lifestylebackendApp:questionTypeUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
