'use strict';

describe('Controller Tests', function() {

    describe('SurveyResults Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSurveyResults;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSurveyResults = jasmine.createSpy('MockSurveyResults');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'SurveyResults': MockSurveyResults
            };
            createController = function() {
                $injector.get('$controller')("SurveyResultsDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'lifestylebackendApp:surveyResultsUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
